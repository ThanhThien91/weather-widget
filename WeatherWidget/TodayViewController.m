//
//  TodayViewController.m
//  WeatherWidget
//
//  Created by Thanh Thien on 8/24/14.
//  Copyright (c) 2014 Ryan Nystrom. All rights reserved.
//

#import "TodayViewController.h"
#import <NotificationCenter/NotificationCenter.h>
#import "WXCondition.h"
#import "AsyncImageView.h"


@interface TodayViewController () <NCWidgetProviding>
{
    NSUserDefaults *dataShareFromContainApp;
}
@end

@implementation TodayViewController



- (void)viewDidLoad {
    
    [self setPreferredContentSize:CGSizeMake(self.view.frame.size.width , self.view.frame.size.height)];
    
    [_lblWeatherCondition setFont:FONT_WEATHER_REGULAR(90)];
    [_lblWeatherCondition setTextColor:FONT_COLOR(whiteColor)];
    [_lblWeatherCondition setText:@"Q"];
    
    //  Background Weather Condition
    [self roundImageView:_backgroundWeatherCondition andColor:[UIColor colorWithRed:91/255.0f green:187/255.0f blue:245/255.0f alpha:1.0f] andBorderWidth:kBorderWidthBackgroundWeatherCondition];
    //  background Temp
    [self roundImageView:_backgroundTemp andColor:[UIColor colorWithRed:255/255.0f green:68/255.0f blue:81/255.0f alpha:1.0f] andBorderWidth:kBorderWidthBackgroundTemp];
    
    [super viewDidLoad];
    
    
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)widgetPerformUpdateWithCompletionHandler:(void (^)(NCUpdateResult))completionHandler {
    // Perform any setup necessary in order to update the view.
    
    // If an error is encoutered, use NCUpdateResultFailed
    // If there's no update required, use NCUpdateResultNoData
    // If there's an update, use NCUpdateResultNewData
    NSUserDefaults *dataSend = [[NSUserDefaults alloc] initWithSuiteName:@"group.weatherwidget"];
    if (![dataSend isEqual:dataShareFromContainApp]) {
        dataShareFromContainApp = dataSend;
        [self updateWeatherCondition];
        completionHandler(NCUpdateResultNewData);
    }
    else
    {
        completionHandler(NCUpdateResultNoData);
    }

}



/**
 * border image
 *
 * @param <#param#>
 * @returns <#returns#>
 */
-(UIImageView *)roundImageView: (UIImageView *)imageView andColor:(UIColor *)color andBorderWidth:(float)borderWidth
{
    imageView.layer.masksToBounds = YES;
    imageView.layer.cornerRadius = imageView.frame.size.width / 2;
    imageView.layer.borderColor = color.CGColor;
    imageView.layer.borderWidth = borderWidth;
    imageView.layer.rasterizationScale = [UIScreen mainScreen].scale;
    imageView.layer.shouldRasterize = YES;
    imageView.clipsToBounds = YES;
    return imageView;
}



/********************************************************************************/
#pragma mark - Update Data


- (void)updateWeatherCondition {
    NSData *data = [dataShareFromContainApp objectForKey:@"currentCondition"];
    WXCondition *currentCondition = [[NSKeyedUnarchiver unarchiveObjectWithData:data] firstObject];
    _lblCityName.text = currentCondition.locationName;
    _lblTemp.text = [currentCondition.temperature stringValue];
    _lblCondition.text = currentCondition.condition;
    _lblCondition
}




/**
 * match icon and condition
 *
 * @param <#param#>
 * @returns <#returns#>
 */
- (NSDictionary *)imageMap {
    static NSDictionary *_imageMap = nil;
    if (! _imageMap) {
        _imageMap = @{
                      @"weather-clear" : @"A",
                      @"weather-few" : @"C",
                      @"weather-few" : @"C",
                      @"weather-broken" : @"P",
                      @"weather-shower" : @"X",
                      @"weather-rain" : @"F",
                      @"weather-tstorm" : @"T",
                      @"weather-snow" : @"W",
                      @"weather-mist" : @"G",
                      @"weather-moon" : @"I",
                      @"weather-few-night" : @"J",
                      @"weather-few-night" : @"J",
                      @"weather-broken" : @"P",
                      @"weather-shower" : @"X",
                      @"weather-rain-night" : @"K",
                      @"weather-tstorm" : @"T",
                      @"weather-snow" : @"W",
                      @"weather-mist" : @"G",
                      };
    }
    return _imageMap;
}



/**
 * get icon condition
 *
 * @param <#param#>
 * @returns <#returns#>
 */
- (NSString *)imageName:(NSString *)icon {
    return [self imageMap][icon];
}



/********************************************************************************/



/********************************************************************************/
#pragma mark - UITableView Datasource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 10;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"Cell";
    
    UITableViewCell *cell = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if(cell == nil)
    {
        //  Custom
        //NSArray *nib = [[NSBundle mainBundle] loadNibNamed:cellIdentifier owner:self options:nil];
        //cell = [nib objectAtIndex:0];
        
        //  Default
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        
    }
    
    cell.textLabel.text = [NSString stringWithFormat:@"Cell %ld", (long)indexPath.row];
    
    return cell;
}


/********************************************************************************/


/********************************************************************************/
#pragma mark UITableView Delegate methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
}


/********************************************************************************/

@end
