//
//  TodayViewController.h
//  WeatherWidget
//
//  Created by Thanh Thien on 8/24/14.
//  Copyright (c) 2014 Ryan Nystrom. All rights reserved.
//

#import <UIKit/UIKit.h>


#define FONT_FAMILY @"Weather&Time"
#define FONT_WEATHER_REGULAR(s) [UIFont fontWithName:FONT_FAMILY size:s]
#define FONT_COLOR(color) [UIColor color]
#define kBorderWidthBackgroundWeatherCondition 17
#define kBorderWidthBackgroundTemp 0

@interface TodayViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITableView *tableViewWeek;



//  current condition
@property (weak, nonatomic) IBOutlet UILabel *lblWeatherCondition;
@property (weak, nonatomic) IBOutlet UIImageView *backgroundWeatherCondition;
@property (weak, nonatomic) IBOutlet UIImageView *backgroundTemp;
@property (weak, nonatomic) IBOutlet UILabel *lblCityName;
@property (weak, nonatomic) IBOutlet UILabel *lblDate;
@property (weak, nonatomic) IBOutlet UILabel *lblTemp;
@property (weak, nonatomic) IBOutlet UILabel *lblCondition;




@end
